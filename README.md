# powerful-type.vim
A way to type powerfully which doesn't crash your keyboard.  

Inspired by [codeinthedark/editor/pull/1](https://github.com/codeinthedark/editor/pull/1).  
Forked from [gist:rhysd/pawaa_mo-do.vim](https://gist.github.com/rhysd/5681b4c222c49edcf155).  

## Demo
![gif](files/demo.gif)

## Usage
Do `:PowerfulOn` to turn on and `:PowerfulOff` to turn off.  
Do `let g:powerful_type#amplitude = AMPLITUDE` to change the amplitude.  

## License
MIT
